<link rel="stylesheet" href="reset.css" />
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>

<style>
    .htmlEditorContainer {
        width: 40%;
        margin: 0;
        padding: 0;
        border: 1px solid #d0d0d0;
    }
    ul.htmlEditorTools {
        margin: 0;
        padding: 3px;
        background-color: #f5f5f5;
        list-style-type: none;
    }
    ul.htmlEditorTools li {
        display: inline;
        margin: 0;
        padding: 0;
    }
    .htmlEditorTools img {
        margin: 0;
        padding: 0;
        cursor: pointer;
        border: 1px solid #f5f5f5;
        border-radius: 2px;
    }
    .htmlEditorTools img:hover {
        border: 1px solid #cdcdcd;
    }
    .htmlEditorTools img.selected {
        background-color: #ebebeb;
        border: 1px solid #cdcdcd;
    }
    .htmlEditorTools select {
        border: 1px solid #cdcdcd;
        font-size: 11px;
    }
</style>

<script>
    
    $(window).ready(function() {
        $("textarea").htmlEditor({
            width: "40%"
        });
    });

function designModeOn(id) {
    document.getElementById(id).contentWindow.document.designMode = "On";
}

(function($) {
    $.fn.htmlEditor = function(options) {
        // valori di default
        var config = {
            "width": "100%", // rihiamo il val con config.output
            "height": "200px"
        };

        if(options) {
            $.extend(config, options);
        }

        function execCommand(button, command, value) {
            if(value == undefined) {
                value = "";
            }
            iframeid = $(button).closest(".htmlEditorContainer").find("iframe").first().attr("id");
            var edit = document.getElementById(iframeid).contentWindow;
            edit.focus();
            edit.document.execCommand(command, false, value);
            edit.focus();
        }
        
        // per inserire html (ie non lo supporta nativamente, quindi l'html viene appeso in coda e non sul cursore)
        function insertHtml(object, html) {
            if(html != null) {
                if(/MSIE (\d+\.\d+);/.test(navigator.userAgent)) { // IE
                    iframeid = $(object).closest(".htmlEditorContainer").find("iframe").first().attr("id");
                    iframeWindow = document.getElementById(iframeid).contentWindow;
                    iframeBody = iframeWindow.document.body;
            
                    /*
                     * var range = document.getElementById("iframe").contentWindow.document.selection.createRange();
                     * range.pasteHTML("<span style='color: red'>" + range.htmlText + "</span>");
                     * range.htmlText per avere la stringa selezionata
                     */
                    var range = iframeWindow.document.selection.createRange();
                    if(range.htmlText != "") {
                        range.pasteHTML(html); // incolla dove c'è la selezione
                    } else {
                        iframeBody.innerHTML = iframeBody.innerHTML + html; // concatena
                    }
            
                    iframeWindow.focus();
                } else { // FF, Chrome
                    execCommand(object, "inserthtml", html);
                }
            }
        }
        
        this.each(function() {
            
            id = $(this).attr("id");
            name = $(this).attr("name");
            
            $(this).closest("form").submit(function() {
                
                htmlEditors = $(".htmlEditor");
                
                if(htmlEditors.length == 0) {
                    return true;
                }
                
                $.each(htmlEditors, function() {
                    iframeid = $(this).attr("id");
                    html = document.getElementById(iframeid).contentWindow.document.body.innerHTML;
                    
                    hidden = $("<input type='hidden' name='"+iframeid+"' />").val(html);
                    $(this).closest(".htmlEditorContainer").replaceWith(hidden);
                });
                
                $(this).submit();
            });
            
            temp = "<div class='htmlEditorContainer'>";
            temp += "<ul class='htmlEditorTools'>";
            temp += "<li><img src='img/html.gif' class='html' alt='View HTML' title='View HTML' /></li>";
            temp += "<li><img src='img/bold.gif' class='bold' alt='Bold' title='Bold' /></li>";
            temp += "<li><img src='img/italic.gif' class='italic' alt='Italic' title='Italic' /></li>";
            temp += "<li><img src='img/underline.gif' class='underline' alt='Underline' title='Underline' /></li>";
            temp += "<li><img src='img/forecolor.gif' class='forecolor' alt='Foreground color' title='Foreground color' /></li>";
            temp += "<li><img src='img/backcolor.gif' class='backcolor' alt='Background color' title='Background color' /></li>";
            temp += "<li><img src='img/link.gif' class='link' alt='Link' title='Link' /></li>";
            temp += "<li><img src='img/ordlist.gif' class='ordlist' alt='Ordered list' title='Ordered list' /></li>";
            temp += "<li><img src='img/unordlist.gif' class='unordlist' alt='Unordered list' title='Unordered list' /></li>";
            temp += "<li><img src='img/outdent.gif' class='outdent' alt='Outdent' title='Outdent' /></li>";
            temp += "<li><img src='img/indent.gif' class='indent' alt='Indent' title='Indent' /></li>";
            temp += "<li><img src='img/left.gif' class='left selected' alt='Left align' title='Left align' /></li>";
            temp += "<li><img src='img/center.gif' class='center' alt='Center align' title='Center align' /></li>";
            temp += "<li><img src='img/right.gif' class='right' alt='Right align' title='Right align' /></li>";
            temp += "<li><img src='img/justify.gif' class='justify' alt='Justify' title='Justify' /></li>";
            temp += "<li><img src='img/image.gif' class='image' alt='Insert image' title='Insert image' /></li>";
            temp += "<li><img src='img/image.gif' class='prova' alt='Insert image' title='Insert image' /></li>";
            temp += "</ul>";
            temp += "<ul class='htmlEditorTools'>";
            temp += "<li><select class='fontname' alt='Select font' title='Select font'><option value=''>-- Font --</option><option value='Arial'>Arial</option><option value='Courier'>Courier</option><option value='Sans Serif'>Sans Serif</option><option value='Tahoma'>Tahoma</option><option value='Verdana'>Verdana</option><option value='Calibri'>Calibri</option></select></li>";
            temp += "<li><select class='fontsize' alt='Font size' title='Font size'><option value=''>-- Size --</option><option value='1'>Very Small</option><option value='2'>Small</option><option value='3'>Medium</option><option value='4'>Large</option><option value='5'>Larger</option><option value='6'>Very Large</option></select></li>";
            temp += "<li><select class='formatblock' alt='Format block' title='Format block'><option value=''>-- Heading --</option><option value='Heading 1'>H1</option><option value='Heading 2'>H2</option><option value='Heading 3'>H3</option><option value='Heading 4'>H4</option><option value='Heading 5'>H5</option><option value='Heading 6'>H6</option></select></li>";
            temp += "</ul>";
            temp += "<iframe name='"+name+"' id='"+id+"' class='htmlEditor' frameBorder='0'></iframe>";
            temp += "</div>";
            
            $(this).replaceWith(temp);
            
            $(".htmlEditorContainer").width(config.width);
            
            // Attivo l'iframe in design mode
            iframeElement = document.getElementById(id);
            iframeEditor = iframeElement.contentWindow.document;
            iframeEditor.designMode = "On";
            setTimeout("designModeOn('"+id+"')", 10); // per firefox
            iframeEditor.close();

            $(iframeElement).width("100%").height(config.height);
            
            // imposto justify left
            iframeElement.contentWindow.document.execCommand("justifyleft", false, "");
        });
        
        // Gestione grassetto
        $(".bold").click(function(){
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "bold");
        });
        
        // Gestione corsivo
        $(".italic").click(function() {
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "italic");
        });
            
        // Gestione sottolineatura
        $(".underline").click(function() {
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "underline");
        });

        // Gestione forecolor
        $(".forecolor").click(function(e) {
            fCol = prompt("Enter foreground color", "");
            if(fCol != null) {
                execCommand(this, "forecolor", fCol);
            }
        });
        
        // Gestione backcolor
        $(".backcolor").click(function() {
            fCol = prompt("Enter background color", "");
            if(fCol != null) {
                execCommand(this, "backcolor", fCol);
            }
        });
        
        // Gestione link
        $(".link").click(function() {
            url = prompt("Enter link url", "");
            if(url != null) {
                execCommand(this, "createlink", url);
            }
        });
        
        // Gestione ordered list
        $(".ordlist").click(function(){
            execCommand(this, "insertorderedlist");
        });

        // Gestione unordered list
        $(".unordlist").click(function(){
            execCommand(this, "insertunorderedlist");
        });

        // Gestione outdent
        $(".outdent").click(function(){
            execCommand(this, "outdent");
        });

        // Gestione indent
        $(".indent").click(function(){
            execCommand(this, "indent");
        });
        
        // Gestione left
        $(".left").click(function(){
            $(".center,.right,.justify").removeClass("selected");
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "justifyleft");
        });
        
        // Gestione center
        $(".center").click(function(){
            $(".left,.right,.justify").removeClass("selected");
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "justifycenter");
        });
        
        // Gestione right
        $(".right").click(function(){
            $(".left,.center,.justify").removeClass("selected");
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "justifyright");
        });
        
        // Gestione justify
        $(".justify").click(function(){
            $(".left,.center,.right").removeClass("selected");
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            execCommand(this, "justifyfull");
        });
        
        // Gestione html
        $(".html").toggle(function(){
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            
            iframeid = $(this).closest(".htmlEditorContainer").children("iframe").first().attr("id");
            iframeWindow = document.getElementById(iframeid).contentWindow;
            iframeBody = iframeWindow.document.body;
            
            iframeWindow.focus();
            iframeBody.innerText = iframeBody.innerHTML;
            iframeWindow.focus();
      
            // Hide all controls
            $(this).closest(".htmlEditorContainer").find("li").children().hide();
            $(this).show();
        }, function() {
            ($(this).hasClass("selected")) ? $(this).removeClass("selected") : $(this).addClass("selected");
            
            iframeid = $(this).closest(".htmlEditorContainer").children("iframe").first().attr("id");
            iframeWindow = document.getElementById(iframeid).contentWindow;
            iframeBody = iframeWindow.document.body;
            
            iframeWindow.focus();
            iframeBody.innerHTML = iframeBody.innerText;
            iframeWindow.focus();
                
            // Show all controls
            $(this).closest(".htmlEditorContainer").find("li").children().show();
        });
        
        // Gestione fontname
        $(".fontname").change(function(){
            execCommand(this, "fontname", $(this).val());
        });
        
        // Gestione font size
        $(".fontsize").change(function(){
            execCommand(this, "fontsize", $(this).val());
        });
        
        // Gestione format block
        $(".formatblock").change(function(){
            execCommand(this, "formatblock", $(this).val());
        });
        
        // Gestione image
        $(".image").click(function(){
            url = prompt("Enter image url", "");
            if(url != null) {
                execCommand(this, "insertimage", url);
            }
        });
        
        //
        $(".prova").click(function(){
            url = prompt("Enter html", "");
            insertHtml(this, url);
            return;
            width = prompt("Enter image width", "200px");
            flo = prompt("Enter image float", "left");
            
            html = "<img src='"+url+"' width='"+width+"' style='float:"+flo+"' />";
            
            insertHtml(this, html);
        });

        return this;
    }
})(jQuery);

</script>

<?
if(isset($_POST['submit']))
{
    echo "<pre>".print_r($_POST, true)."</pre>";
    die();
}
?>

<form method="post">
    <textarea id="editor" name="editor"></textarea><br />
    <br />
    <input type="submit" name="submit" value="salva" />
</form>